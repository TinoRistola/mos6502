
#include "Instruction.h"

constexpr uint8_t _6502::Instruction::instrCycles[];
constexpr uint8_t _6502::Instruction::instrPageCycles[];

uint8_t _6502::Instruction::length() const
{
	switch (mode)
	{
		case _6502::ABSOLUTE:
		case _6502::ABSOLUTE_X:
		case _6502::ABSOLUTE_Y:
			return (uint8_t)3;
		case _6502::IMMEDIATE:
			return (uint8_t)2;
		case _6502::IMPLICIT:
			return (uint8_t)1;
		case _6502::INDIRECT_INDEX:
		case _6502::INDEX_INDIRECT:
			return (uint8_t)2;
		case _6502::INDIRECT:
			return (uint8_t)3;
		case _6502::RELATIVE:
			return (uint8_t)2;
		case _6502::ZEROPAGE:
		case _6502::ZEROPAGE_X:
		case _6502::ZEROPAGE_Y:
			return (uint8_t)2;
	}

	return (uint8_t)-1;
}

uint8_t _6502::Instruction::cycles(uint8_t opcode, bool bPageCrossed)
{
	uint8_t result = _6502::Instruction::instrCycles[opcode];

	if(bPageCrossed)
		result += _6502::Instruction::instrPageCycles[opcode];

	return result;
}
