#pragma once

#include <cstdint>
#include <initializer_list>
#include <bitset>

namespace _6502
{
	class Registers
	{
	public:
		uint16_t PC = 0;

        uint8_t
			A = 0,
			X = 0,
			Y = 0,
			SP = 0xFD;

		enum Flags : size_t
		{
			C, Z, I, D, B, U, V, N
		};

		void reset();

		inline bool getSR(Flags flag) const
		{
			return SR.test(flag);
		}

		inline uint8_t getSR() const
		{
			return static_cast<uint8_t>(SR.to_ulong());
		}

		inline void setSR(Flags flag, bool value)
		{
			SR[flag] = value;
		}

		void setSR(uint16_t result, Flags flag);

		template<typename... F>
		void setSR(uint16_t result, Flags flag, F... flags)
		{
			setSR(result, flag);
			setSR(result, flags...);
		}

		inline void setSR(uint8_t status)
		{
			SR = std::bitset<8>(status | 0x20);
		}

		void setVOnAdd(uint8_t x, uint8_t y, uint8_t result);
		void setVOnSub(uint8_t x, uint8_t y, uint8_t result);

		std::bitset<8> SR = std::bitset<8>(0b00100000);
	};
}
