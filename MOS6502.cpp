#include "MOS6502.h"

namespace _6502
{
	MOS6502::MOS6502() :
		mem(0x10000),
		bus(&mem)
	{
		_instr.resize(0x100);
		_instr[0x61] = { &MOS6502::ADC, "ADC", INDEX_INDIRECT };
		_instr[0x65] = { &MOS6502::ADC, "ADC", ZEROPAGE };
		_instr[0x69] = { &MOS6502::ADC, "ADC", IMMEDIATE };
		_instr[0x6D] = { &MOS6502::ADC, "ADC", ABSOLUTE };
		_instr[0x71] = { &MOS6502::ADC, "ADC", INDIRECT_INDEX };
		_instr[0x75] = { &MOS6502::ADC, "ADC", ZEROPAGE_X };
		_instr[0x79] = { &MOS6502::ADC, "ADC", ABSOLUTE_Y };
		_instr[0x7D] = { &MOS6502::ADC, "ADC", ABSOLUTE_X };
		//_instr[0x93] = { &MOS6502::AHX, "AHX", INDIRECT_INDEX };
		//_instr[0x9F] = { &MOS6502::AHX, "AHX", ABSOLUTE_Y };
		//_instr[0x4B] = { &MOS6502::ALR, "ALR", IMMEDIATE };
		//_instr[0x0B] = { &MOS6502::ANC, "ANC", IMMEDIATE };
		//_instr[0x2B] = { &MOS6502::ANC, "ANC", IMMEDIATE };
		_instr[0x21] = { &MOS6502::AND, "AND", INDEX_INDIRECT };
		_instr[0x25] = { &MOS6502::AND, "AND", ZEROPAGE };
		_instr[0x29] = { &MOS6502::AND, "AND", IMMEDIATE };
		_instr[0x2D] = { &MOS6502::AND, "AND", ABSOLUTE };
		_instr[0x31] = { &MOS6502::AND, "AND", INDIRECT_INDEX };
		_instr[0x35] = { &MOS6502::AND, "AND", ZEROPAGE_X };
		_instr[0x39] = { &MOS6502::AND, "AND", ABSOLUTE_Y };
		_instr[0x3D] = { &MOS6502::AND, "AND", ABSOLUTE_X };
		//_instr[0x6B] = { &MOS6502::ARR, "ARR", IMMEDIATE };
		_instr[0x06] = { &MOS6502::ASL, "ASL", ZEROPAGE };
		_instr[0x0A] = { &MOS6502::ASL, "ASL", IMPLICIT };
		_instr[0x0E] = { &MOS6502::ASL, "ASL", ABSOLUTE };
		_instr[0x16] = { &MOS6502::ASL, "ASL", ZEROPAGE_X };
		_instr[0x1E] = { &MOS6502::ASL, "ASL", ABSOLUTE_X };
		//_instr[0xCB] = { &MOS6502::AXS, "AXS", IMMEDIATE };
		_instr[0x90] = { &MOS6502::BCC, "BCC", RELATIVE };
		_instr[0xB0] = { &MOS6502::BCS, "BCS", RELATIVE };
		_instr[0xF0] = { &MOS6502::BEQ, "BEQ", RELATIVE };
		_instr[0x24] = { &MOS6502::BIT, "BIT", ZEROPAGE };
		_instr[0x2C] = { &MOS6502::BIT, "BIT", ABSOLUTE };
		_instr[0x30] = { &MOS6502::BMI, "BMI", RELATIVE };
		_instr[0xD0] = { &MOS6502::BNE, "BNE", RELATIVE };
		_instr[0x10] = { &MOS6502::BPL, "BPL", RELATIVE };
		_instr[0x00] = { &MOS6502::BRK, "BRK", IMPLICIT };
		_instr[0x50] = { &MOS6502::BVC, "BVC", RELATIVE };
		_instr[0x70] = { &MOS6502::BVS, "BVS", RELATIVE };
		_instr[0x18] = { &MOS6502::CLC, "CLC", IMPLICIT };
		_instr[0xD8] = { &MOS6502::CLD, "CLD", IMPLICIT };
		_instr[0x58] = { &MOS6502::CLI, "CLI", IMPLICIT };
		_instr[0xB8] = { &MOS6502::CLV, "CLV", IMPLICIT };
		_instr[0xC1] = { &MOS6502::CMP, "CMP", INDEX_INDIRECT };
		_instr[0xC5] = { &MOS6502::CMP, "CMP", ZEROPAGE };
		_instr[0xC9] = { &MOS6502::CMP, "CMP", IMMEDIATE };
		_instr[0xCD] = { &MOS6502::CMP, "CMP", ABSOLUTE };
		_instr[0xD1] = { &MOS6502::CMP, "CMP", INDIRECT_INDEX };
		_instr[0xD5] = { &MOS6502::CMP, "CMP", ZEROPAGE_X };
		_instr[0xD9] = { &MOS6502::CMP, "CMP", ABSOLUTE_Y };
		_instr[0xDD] = { &MOS6502::CMP, "CMP", ABSOLUTE_X };
		_instr[0xE0] = { &MOS6502::CPX, "CPX", IMMEDIATE };
		_instr[0xE4] = { &MOS6502::CPX, "CPX", ZEROPAGE };
		_instr[0xEC] = { &MOS6502::CPX, "CPX", ABSOLUTE };
		_instr[0xC0] = { &MOS6502::CPY, "CPY", IMMEDIATE };
		_instr[0xC4] = { &MOS6502::CPY, "CPY", ZEROPAGE };
		_instr[0xCC] = { &MOS6502::CPY, "CPY", ABSOLUTE };
		/*_instr[0xC3] = { &MOS6502::DCP, "DCP", INDEX_INDIRECT };
		_instr[0xC7] = { &MOS6502::DCP, "DCP", ZEROPAGE };
		_instr[0xCF] = { &MOS6502::DCP, "DCP", ABSOLUTE };
		_instr[0xD3] = { &MOS6502::DCP, "DCP", INDIRECT_INDEX };
		_instr[0xD7] = { &MOS6502::DCP, "DCP", ZEROPAGE_X };
		_instr[0xDB] = { &MOS6502::DCP, "DCP", ABSOLUTE_Y };
		_instr[0xDF] = { &MOS6502::DCP, "DCP", ABSOLUTE_X };*/
		_instr[0xC6] = { &MOS6502::DEC, "DEC", ZEROPAGE };
		_instr[0xCE] = { &MOS6502::DEC, "DEC", ABSOLUTE };
		_instr[0xD6] = { &MOS6502::DEC, "DEC", ZEROPAGE_X };
		_instr[0xDE] = { &MOS6502::DEC, "DEC", ABSOLUTE_X };
		_instr[0xCA] = { &MOS6502::DEX, "DEX", IMPLICIT };
		_instr[0x88] = { &MOS6502::DEY, "DEY", IMPLICIT };
		_instr[0x41] = { &MOS6502::EOR, "EOR", INDEX_INDIRECT };
		_instr[0x45] = { &MOS6502::EOR, "EOR", ZEROPAGE };
		_instr[0x49] = { &MOS6502::EOR, "EOR", IMMEDIATE };
		_instr[0x4D] = { &MOS6502::EOR, "EOR", ABSOLUTE };
		_instr[0x51] = { &MOS6502::EOR, "EOR", INDIRECT_INDEX };
		_instr[0x55] = { &MOS6502::EOR, "EOR", ZEROPAGE_X };
		_instr[0x59] = { &MOS6502::EOR, "EOR", ABSOLUTE_Y };
		_instr[0x5D] = { &MOS6502::EOR, "EOR", ABSOLUTE_X };
		_instr[0xE6] = { &MOS6502::INC, "INC", ZEROPAGE };
		_instr[0xEE] = { &MOS6502::INC, "INC", ABSOLUTE };
		_instr[0xF6] = { &MOS6502::INC, "INC", ZEROPAGE_X };
		_instr[0xFE] = { &MOS6502::INC, "INC", ABSOLUTE_X };
		_instr[0xE8] = { &MOS6502::INX, "INX", IMPLICIT };
		_instr[0xC8] = { &MOS6502::INY, "INY", IMPLICIT };
		/*_instr[0xE3] = { &MOS6502::ISC, "ISC", INDEX_INDIRECT };
		_instr[0xE7] = { &MOS6502::ISC, "ISC", ZEROPAGE };
		_instr[0xEF] = { &MOS6502::ISC, "ISC", ABSOLUTE };
		_instr[0xF3] = { &MOS6502::ISC, "ISC", INDIRECT_INDEX };
		_instr[0xF7] = { &MOS6502::ISC, "ISC", ZEROPAGE_X };
		_instr[0xFB] = { &MOS6502::ISC, "ISC", ABSOLUTE_Y };
		_instr[0xFF] = { &MOS6502::ISC, "ISC", ABSOLUTE_X };*/
		_instr[0x4C] = { &MOS6502::JMP, "JMP", ABSOLUTE };
		_instr[0x6C] = { &MOS6502::JMP, "JMP", INDIRECT };
		_instr[0x20] = { &MOS6502::JSR, "JSR", ABSOLUTE };
		/*_instr[0xBB] = { &MOS6502::LAS, "LAS", ABSOLUTE_Y };
		_instr[0xA3] = { &MOS6502::LAX, "LAX", INDEX_INDIRECT };
		_instr[0xA7] = { &MOS6502::LAX, "LAX", ZEROPAGE };
		_instr[0xAB] = { &MOS6502::LAX, "LAX", IMMEDIATE };
		_instr[0xAF] = { &MOS6502::LAX, "LAX", ABSOLUTE };
		_instr[0xB3] = { &MOS6502::LAX, "LAX", INDIRECT_INDEX };
		_instr[0xB7] = { &MOS6502::LAX, "LAX", ZEROPAGE_Y };
		_instr[0xBF] = { &MOS6502::LAX, "LAX", ABSOLUTE_Y };*/
		_instr[0xA1] = { &MOS6502::LDA, "LDA", INDEX_INDIRECT };
		_instr[0xA5] = { &MOS6502::LDA, "LDA", ZEROPAGE };
		_instr[0xA9] = { &MOS6502::LDA, "LDA", IMMEDIATE };
		_instr[0xAD] = { &MOS6502::LDA, "LDA", ABSOLUTE };
		_instr[0xB1] = { &MOS6502::LDA, "LDA", INDIRECT_INDEX };
		_instr[0xB5] = { &MOS6502::LDA, "LDA", ZEROPAGE_X };
		_instr[0xB9] = { &MOS6502::LDA, "LDA", ABSOLUTE_Y };
		_instr[0xBD] = { &MOS6502::LDA, "LDA", ABSOLUTE_X };
		_instr[0xA2] = { &MOS6502::LDX, "LDX", IMMEDIATE };
		_instr[0xA6] = { &MOS6502::LDX, "LDX", ZEROPAGE };
		_instr[0xAE] = { &MOS6502::LDX, "LDX", ABSOLUTE };
		_instr[0xB6] = { &MOS6502::LDX, "LDX", ZEROPAGE_Y };
		_instr[0xBE] = { &MOS6502::LDX, "LDX", ABSOLUTE_Y };
		_instr[0xA0] = { &MOS6502::LDY, "LDY", IMMEDIATE };
		_instr[0xA4] = { &MOS6502::LDY, "LDY", ZEROPAGE };
		_instr[0xAC] = { &MOS6502::LDY, "LDY", ABSOLUTE };
		_instr[0xB4] = { &MOS6502::LDY, "LDY", ZEROPAGE_X };
		_instr[0xBC] = { &MOS6502::LDY, "LDY", ABSOLUTE_X };
		_instr[0x46] = { &MOS6502::LSR, "LSR", ZEROPAGE };
		_instr[0x4A] = { &MOS6502::LSR, "LSR", IMPLICIT };
		_instr[0x4E] = { &MOS6502::LSR, "LSR", ABSOLUTE };
		_instr[0x56] = { &MOS6502::LSR, "LSR", ZEROPAGE_X };
		_instr[0x5E] = { &MOS6502::LSR, "LSR", ABSOLUTE_X };
		//_instr[0x04] = { &MOS6502::NOP, "NOP", ZEROPAGE };
		//_instr[0x0C] = { &MOS6502::NOP, "NOP", ABSOLUTE };
		//_instr[0x14] = { &MOS6502::NOP, "NOP", ZEROPAGE_X };
		//_instr[0x1A] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0x1C] = { &MOS6502::NOP, "NOP", ABSOLUTE_X };
		//_instr[0x34] = { &MOS6502::NOP, "NOP", ZEROPAGE_X };
		//_instr[0x3A] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0x3C] = { &MOS6502::NOP, "NOP", ABSOLUTE_X };
		//_instr[0x44] = { &MOS6502::NOP, "NOP", ZEROPAGE };
		//_instr[0x54] = { &MOS6502::NOP, "NOP", ZEROPAGE_X };
		//_instr[0x5A] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0x5C] = { &MOS6502::NOP, "NOP", ABSOLUTE_X };
		//_instr[0x64] = { &MOS6502::NOP, "NOP", ZEROPAGE };
		//_instr[0x74] = { &MOS6502::NOP, "NOP", ZEROPAGE_X };
		//_instr[0x7A] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0x7C] = { &MOS6502::NOP, "NOP", ABSOLUTE_X };
		//_instr[0x80] = { &MOS6502::NOP, "NOP", IMMEDIATE };
		//_instr[0x82] = { &MOS6502::NOP, "NOP", IMMEDIATE };
		//_instr[0x89] = { &MOS6502::NOP, "NOP", IMMEDIATE };
		//_instr[0xC2] = { &MOS6502::NOP, "NOP", IMMEDIATE };
		//_instr[0xD4] = { &MOS6502::NOP, "NOP", ZEROPAGE_X };
		//_instr[0xDA] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0xDC] = { &MOS6502::NOP, "NOP", ABSOLUTE_X };
		//_instr[0xE2] = { &MOS6502::NOP, "NOP", IMMEDIATE };
		_instr[0xEA] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0xF4] = { &MOS6502::NOP, "NOP", ZEROPAGE_X };
		//_instr[0xFA] = { &MOS6502::NOP, "NOP", IMPLICIT };
		//_instr[0xFC] = { &MOS6502::NOP, "NOP", ABSOLUTE_X };
		_instr[0x01] = { &MOS6502::ORA, "ORA", INDEX_INDIRECT };
		_instr[0x05] = { &MOS6502::ORA, "ORA", ZEROPAGE };
		_instr[0x09] = { &MOS6502::ORA, "ORA", IMMEDIATE };
		_instr[0x0D] = { &MOS6502::ORA, "ORA", ABSOLUTE };
		_instr[0x11] = { &MOS6502::ORA, "ORA", INDIRECT_INDEX };
		_instr[0x15] = { &MOS6502::ORA, "ORA", ZEROPAGE_X };
		_instr[0x19] = { &MOS6502::ORA, "ORA", ABSOLUTE_Y };
		_instr[0x1D] = { &MOS6502::ORA, "ORA", ABSOLUTE_X };
		_instr[0x48] = { &MOS6502::PHA, "PHA", IMPLICIT };
		_instr[0x08] = { &MOS6502::PHP, "PHP", IMPLICIT };
		_instr[0x68] = { &MOS6502::PLA, "PLA", IMPLICIT };
		_instr[0x28] = { &MOS6502::PLP, "PLP", IMPLICIT };
		//_instr[0x23] = { &MOS6502::RLA, "RLA", INDEX_INDIRECT };
		//_instr[0x27] = { &MOS6502::RLA, "RLA", ZEROPAGE };
		//_instr[0x2F] = { &MOS6502::RLA, "RLA", ABSOLUTE };
		//_instr[0x33] = { &MOS6502::RLA, "RLA", INDIRECT_INDEX };
		//_instr[0x37] = { &MOS6502::RLA, "RLA", ZEROPAGE_X };
		//_instr[0x3B] = { &MOS6502::RLA, "RLA", ABSOLUTE_Y };
		//_instr[0x3F] = { &MOS6502::RLA, "RLA", ABSOLUTE_X };
		_instr[0x26] = { &MOS6502::ROL, "ROL", ZEROPAGE };
		_instr[0x2A] = { &MOS6502::ROL, "ROL", IMPLICIT };
		_instr[0x2E] = { &MOS6502::ROL, "ROL", ABSOLUTE };
		_instr[0x36] = { &MOS6502::ROL, "ROL", ZEROPAGE_X };
		_instr[0x3E] = { &MOS6502::ROL, "ROL", ABSOLUTE_X };
		_instr[0x66] = { &MOS6502::ROR, "ROR", ZEROPAGE };
		_instr[0x6A] = { &MOS6502::ROR, "ROR", IMPLICIT };
		_instr[0x6E] = { &MOS6502::ROR, "ROR", ABSOLUTE };
		_instr[0x76] = { &MOS6502::ROR, "ROR", ZEROPAGE_X };
		_instr[0x7E] = { &MOS6502::ROR, "ROR", ABSOLUTE_X };
		//_instr[0x63] = { &MOS6502::RRA, "RRA", INDEX_INDIRECT };
		//_instr[0x67] = { &MOS6502::RRA, "RRA", ZEROPAGE };
		//_instr[0x6F] = { &MOS6502::RRA, "RRA", ABSOLUTE };
		//_instr[0x73] = { &MOS6502::RRA, "RRA", INDIRECT_INDEX };
		//_instr[0x77] = { &MOS6502::RRA, "RRA", ZEROPAGE_X };
		//_instr[0x7B] = { &MOS6502::RRA, "RRA", ABSOLUTE_Y };
		//_instr[0x7F] = { &MOS6502::RRA, "RRA", ABSOLUTE_X };
		_instr[0x40] = { &MOS6502::RTI, "RTI", IMPLICIT };
		_instr[0x60] = { &MOS6502::RTS, "RTS", IMPLICIT };
		//_instr[0x83] = { &MOS6502::SAX, "SAX", INDEX_INDIRECT };
		//_instr[0x87] = { &MOS6502::SAX, "SAX", ZEROPAGE };
		//_instr[0x8F] = { &MOS6502::SAX, "SAX", ABSOLUTE };
		//_instr[0x97] = { &MOS6502::SAX, "SAX", ZEROPAGE_Y };
		_instr[0xE1] = { &MOS6502::SBC, "SBC", INDEX_INDIRECT };
		_instr[0xE5] = { &MOS6502::SBC, "SBC", ZEROPAGE };
		_instr[0xE9] = { &MOS6502::SBC, "SBC", IMMEDIATE };
		//_instr[0xEB] = { &MOS6502::SBC, "SBC", IMMEDIATE };
		_instr[0xED] = { &MOS6502::SBC, "SBC", ABSOLUTE };
		_instr[0xF1] = { &MOS6502::SBC, "SBC", INDIRECT_INDEX };
		_instr[0xF5] = { &MOS6502::SBC, "SBC", ZEROPAGE_X };
		_instr[0xF9] = { &MOS6502::SBC, "SBC", ABSOLUTE_Y };
		_instr[0xFD] = { &MOS6502::SBC, "SBC", ABSOLUTE_X };
		_instr[0x38] = { &MOS6502::SEC, "SEC", IMPLICIT };
		_instr[0xF8] = { &MOS6502::SED, "SED", IMPLICIT };
		_instr[0x78] = { &MOS6502::SEI, "SEI", IMPLICIT };
		//_instr[0x9E] = { &MOS6502::SHX, "SHX", ABSOLUTE_Y };
		//_instr[0x9C] = { &MOS6502::SHY, "SHY", ABSOLUTE_X };
		//_instr[0x03] = { &MOS6502::SLO, "SLO", INDEX_INDIRECT };
		//_instr[0x07] = { &MOS6502::SLO, "SLO", ZEROPAGE };
		//_instr[0x0F] = { &MOS6502::SLO, "SLO", ABSOLUTE };
		//_instr[0x13] = { &MOS6502::SLO, "SLO", INDIRECT_INDEX };
		//_instr[0x17] = { &MOS6502::SLO, "SLO", ZEROPAGE_X };
		//_instr[0x1B] = { &MOS6502::SLO, "SLO", ABSOLUTE_Y };
		//_instr[0x1F] = { &MOS6502::SLO, "SLO", ABSOLUTE_X };
		//_instr[0x43] = { &MOS6502::SRE, "SRE", INDEX_INDIRECT };
		//_instr[0x47] = { &MOS6502::SRE, "SRE", ZEROPAGE };
		//_instr[0x4F] = { &MOS6502::SRE, "SRE", ABSOLUTE };
		//_instr[0x53] = { &MOS6502::SRE, "SRE", INDIRECT_INDEX };
		//_instr[0x57] = { &MOS6502::SRE, "SRE", ZEROPAGE_X };
		//_instr[0x5B] = { &MOS6502::SRE, "SRE", ABSOLUTE_Y };
		//_instr[0x5F] = { &MOS6502::SRE, "SRE", ABSOLUTE_X };
		_instr[0x81] = { &MOS6502::STA, "STA", INDEX_INDIRECT };
		_instr[0x85] = { &MOS6502::STA, "STA", ZEROPAGE };
		_instr[0x8D] = { &MOS6502::STA, "STA", ABSOLUTE };
		_instr[0x91] = { &MOS6502::STA, "STA", INDIRECT_INDEX };
		_instr[0x95] = { &MOS6502::STA, "STA", ZEROPAGE_X };
		_instr[0x99] = { &MOS6502::STA, "STA", ABSOLUTE_Y };
		_instr[0x9D] = { &MOS6502::STA, "STA", ABSOLUTE_X };
		//_instr[0x02] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x12] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x22] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x32] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x42] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x52] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x62] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x72] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0x92] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0xB2] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0xD2] = { &MOS6502::STP, "STP", IMPLICIT };
		//_instr[0xF2] = { &MOS6502::STP, "STP", IMPLICIT };
		_instr[0x86] = { &MOS6502::STX, "STX", ZEROPAGE };
		_instr[0x8E] = { &MOS6502::STX, "STX", ABSOLUTE };
		_instr[0x96] = { &MOS6502::STX, "STX", ZEROPAGE_Y };
		_instr[0x84] = { &MOS6502::STY, "STY", ZEROPAGE };
		_instr[0x8C] = { &MOS6502::STY, "STY", ABSOLUTE };
		_instr[0x94] = { &MOS6502::STY, "STY", ZEROPAGE_X };
		//_instr[0x9B] = { &MOS6502::TAS, "TAS", ABSOLUTE_Y };
		_instr[0xAA] = { &MOS6502::TAX, "TAX", IMPLICIT };
		_instr[0xA8] = { &MOS6502::TAY, "TAY", IMPLICIT };
		_instr[0xBA] = { &MOS6502::TSX, "TSX", IMPLICIT };
		_instr[0x8A] = { &MOS6502::TXA, "TXA", IMPLICIT };
		_instr[0x9A] = { &MOS6502::TXS, "TXS", IMPLICIT };
		_instr[0x98] = { &MOS6502::TYA, "TYA", IMPLICIT };
		//_instr[0x8B] = { &MOS6502::XAA, "XAA", IMMEDIATE };

		reset();
	}

	void MOS6502::step()
	{
		if(_cycles > 0)
		{
			_cycles--;
			return;
		}

		const uint8_t opcode = bus.read(reg.PC);
		const Instruction* i = &_instr[opcode];

		if (i && i->func)
		{
			uint16_t dst, value;
			bool bPageCrossed = false;

			getOPInfo(*i, dst, value, bPageCrossed);

			reg.PC += i->length();
			_cycles = Instruction::cycles(opcode, bPageCrossed);

			(this->*i->func)(dst, value, i->mode);
		}
		else
		{
			std::cerr << "Illegal opcode not supported: 0x" << std::hex << static_cast<uint16_t>(opcode) << std::endl;
			exit(1);
		}
	}

	void MOS6502::load(const char * fileName)
	{
		_load(fileName);
		reg.PC = bus.readAddr(RSTVector);
	}

	void MOS6502::load(const char * fileName, uint16_t startAddr)
	{
		_load(fileName);
		reg.PC = startAddr;
	}

	void MOS6502::_load(const char * fileName)
	{
		std::ifstream ifs(fileName, std::ios::binary | std::ios::ate);
		auto pos = ifs.tellg();

		ifs.seekg(0, std::ios::beg);
		ifs.read(reinterpret_cast<char*>(mem.get()), pos);
		ifs.close();
	}

	void MOS6502::stackPush(uint8_t value)
	{
		bus.write(StackBegin | reg.SP--, value);
	}

	void MOS6502::stackPushAddr(uint16_t value)
	{
		stackPush(value >> 8);
		stackPush(value & 0xFF);
	}

	uint8_t MOS6502::stackPop()
	{
		return bus.read(StackBegin | static_cast<uint16_t>(++reg.SP));
	}

	uint16_t MOS6502::stackPopAddr()
	{
		return stackPop() | (stackPop() << 8);
	}

	void MOS6502::NMI()
	{
		reg.setSR(Registers::B, false);
		stackPushAddr(reg.PC);
		stackPush(reg.getSR());
		reg.setSR(Registers::I, true);
		reg.PC = bus.readAddr(NMIVector);
		_cycles += 7;
	}

	void MOS6502::IRQ()
	{
		reg.setSR(Registers::B, false);
		stackPushAddr(reg.PC);
		stackPush(reg.getSR());
		reg.setSR(Registers::I, true);
		reg.PC = bus.readAddr(IRQVector);
		_cycles += 7;
	}

	void MOS6502::reset()
	{
		reg.reset();
		reg.setSR(Registers::I, true);
		reg.PC = bus.readAddr(RSTVector);
		_cycles = 6;
	}

	bool MOS6502::differentPages(uint16_t a, uint16_t b) const
	{
		return (a & 0xFF00) != (b & 0xFF00);
	}

	void MOS6502::addBranchCycles(uint16_t addr)
	{
		_cycles++;

		if(differentPages(reg.PC, addr))
			_cycles++;
	}

	void MOS6502::getOPInfo(const Instruction& i, uint16_t& dst, uint16_t& value, bool& bPageCrossed)
	{
		if (i.length() < 3)
			dst = bus.read(reg.PC + 1);
		else
			dst = bus.readAddr(reg.PC + 1);

		switch (i.mode)
		{
			case _6502::ABSOLUTE:
				value = bus.read(dst);
				break;
			case _6502::ABSOLUTE_X:
				dst += reg.X;
				value = bus.read(dst);
				bPageCrossed = differentPages(dst - reg.X, dst);
				break;
			case _6502::ABSOLUTE_Y:
				dst += reg.Y;
				value = bus.read(dst);
				bPageCrossed = differentPages(dst - reg.Y, dst);
				break;
			case _6502::IMMEDIATE:
				value = dst;
				break;
			case _6502::INDEX_INDIRECT:
			{
				dst = bus.readAddr((dst + reg.X) % 256);
				value = bus.read(dst);
				break;
			}
			case _6502::INDIRECT:
				dst = bus.readAddr(dst);
				break;
			case _6502::INDIRECT_INDEX:
			{
				dst = bus.readAddr(dst) + reg.Y;
				value = bus.read(dst);
				bPageCrossed = differentPages(dst - reg.Y, dst);
			}
			break;
			case _6502::RELATIVE:
			{
				int16_t offset = static_cast<int16_t>(dst);

				if (offset & 0x80)
					offset |= 0xFF00;

				dst = reg.PC + offset + 2;
				break;
			}
			case _6502::ZEROPAGE:
				dst %= 256;
				value = bus.read(dst);
				break;
			case _6502::ZEROPAGE_X:
				dst += reg.X;
				dst %= 256;
				value = bus.read(dst);
				break;
			case _6502::ZEROPAGE_Y:
				dst += reg.Y;
				dst %= 256;
				value = bus.read(dst);
				break;
		}
	}

	void MOS6502::ADC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		uint16_t result = reg.A + value + (reg.getSR(Registers::C) ? 1 : 0);

		if (reg.getSR(Registers::D))
		{
			if (((reg.A & 0xF) + (value & 0xF) + (reg.getSR(Registers::C) ? 1 : 0)) > 9)
				result += 6;

			reg.setSR(result, Registers::N);

			if (result > 0x99)
				result += 96;

			reg.setSR(Registers::C, result > 0x99);
		}
		else
			reg.setSR(result, Registers::C);

		reg.setVOnAdd(reg.A, static_cast<uint8_t>(value), static_cast<uint8_t>(result));
		reg.setSR(result & 0xFF, Registers::Z, Registers::N);

		reg.A = static_cast<uint8_t>(result);
	}

	void MOS6502::AHX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::ALR(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::ANC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::AND(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A &= value;
		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::ARR(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::ASL(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (addrMode == IMPLICIT)
			value = reg.A;

		uint16_t result = value << 1;

		reg.setSR(result, Registers::Z, Registers::N);
		reg.setSR(Registers::C, (value & 0x80) != 0);

		if (addrMode == IMPLICIT)
			reg.A = static_cast<uint8_t>(result);
		else
			bus.write(dst, static_cast<uint8_t>(result));
	}

	void MOS6502::AXS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::BCC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (!reg.getSR(Registers::C))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BCS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (reg.getSR(Registers::C))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BEQ(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (reg.getSR(Registers::Z))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BIT(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(value, Registers::V, Registers::N);
		reg.setSR(static_cast<uint16_t>(value & reg.A), Registers::Z);
	}

	void MOS6502::BMI(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (reg.getSR(Registers::N))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BNE(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (!reg.getSR(Registers::Z))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BPL(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (!reg.getSR(Registers::N))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BRK(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		stackPushAddr(reg.PC + 1);

		PHP(dst, value, addrMode);
		SEI(dst, value, addrMode);

		reg.PC = bus.readAddr(IRQVector);
	}

	void MOS6502::BVC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (!reg.getSR(Registers::V))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::BVS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (reg.getSR(Registers::V))
		{
			addBranchCycles(dst);
			reg.PC = dst;
		}
	}

	void MOS6502::CLC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::C, false);
	}

	void MOS6502::CLD(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::D, false);
	}

	void MOS6502::CLI(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::I, false);
	}

	void MOS6502::CLV(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::V, false);
	}

	void MOS6502::CMP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::C, reg.A >= value);
		reg.setSR(reg.A - value, Registers::Z, Registers::N);
	}

	void MOS6502::CPX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::C, reg.X >= value);
		reg.setSR(reg.X - value, Registers::Z, Registers::N);
	}

	void MOS6502::CPY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::C, reg.Y >= value);
		reg.setSR(reg.Y - value, Registers::Z, Registers::N);
	}

	void MOS6502::DCP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::DEC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		uint8_t val = value - 1;

		reg.setSR(val, Registers::Z, Registers::N);

		bus.write(dst, val);
	}

	void MOS6502::DEX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(--reg.X, Registers::Z, Registers::N);
	}

	void MOS6502::DEY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(--reg.Y, Registers::Z, Registers::N);
	}

	void MOS6502::EOR(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A ^= value;

		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::INC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		uint8_t val = value + 1;

		reg.setSR(val, Registers::Z, Registers::N);

		bus.write(dst, val);
	}

	void MOS6502::INX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(++reg.X, Registers::Z, Registers::N);
	}

	void MOS6502::INY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(++reg.Y, Registers::Z, Registers::N);
	}

	void MOS6502::ISC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::JMP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.PC = dst;
	}

	void MOS6502::JSR(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		stackPushAddr(reg.PC - 1);
		reg.PC = dst;
	}

	void MOS6502::LAS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::LAX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::LDA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A = static_cast<uint8_t>(value);
		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::LDX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.X = static_cast<uint8_t>(value);
		reg.setSR(reg.X, Registers::Z, Registers::N);
	}

	void MOS6502::LDY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.Y = static_cast<uint8_t>(value);
		reg.setSR(reg.Y, Registers::Z, Registers::N);
	}

	void MOS6502::LSR(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (addrMode == IMPLICIT)
			value = reg.A;

		uint16_t result = value >> 1;

		reg.setSR(result, Registers::Z, Registers::N);
		reg.setSR(Registers::C, (value & 1) != 0);

		if (addrMode == IMPLICIT)
			reg.A = static_cast<uint8_t>(result);
		else
			bus.write(dst, static_cast<uint8_t>(result));
	}

	void MOS6502::NOP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::ORA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A |= value;

		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::PHA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		stackPush(reg.A);
	}

	void MOS6502::PHP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		stackPush(reg.getSR() | 0x10);
	}

	void MOS6502::PLA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A = stackPop();
		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::PLP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(stackPop());
	}

	void MOS6502::RLA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::ROL(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (addrMode == IMPLICIT)
			value = reg.A;

		value <<= 1;

		if (reg.getSR(Registers::C))
			value |= 1;

		reg.setSR(value, Registers::C, Registers::Z, Registers::N);

		if (addrMode == IMPLICIT)
			reg.A = static_cast<uint8_t>(value);
		else
			bus.write(dst, static_cast<uint8_t>(value));
	}

	void MOS6502::ROR(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		if (addrMode == IMPLICIT)
			value = reg.A;

		if (reg.getSR(Registers::C))
			value |= 0x0100;

		reg.setSR(Registers::C, (value & 1) != 0);

		value >>= 1;

		reg.setSR(value, Registers::Z, Registers::N);

		if (addrMode == IMPLICIT)
			reg.A = static_cast<uint8_t>(value);
		else
			bus.write(dst, static_cast<uint8_t>(value));
	}

	void MOS6502::RRA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::RTI(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(stackPop());
		reg.PC = stackPopAddr();
	}

	void MOS6502::RTS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.PC = stackPopAddr() + 1;
	}

	void MOS6502::SAX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::SBC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		uint16_t result = reg.A - value - (reg.getSR(Registers::C) ? 0 : 1);

		if (reg.getSR(Registers::D))
		{
			if ((reg.A & 0xF) - (reg.getSR(Registers::C) ? 0 : 1) < (value & 0xF))
				result -= 6;

			if (result > 0x99)
				result -= 96;
		}

		reg.setVOnSub(reg.A, static_cast<uint8_t>(value), static_cast<uint8_t>(result));
		reg.setSR(result & 0xFF, Registers::Z, Registers::N);
		reg.setSR(Registers::C, result < 0x100);

		reg.A = static_cast<uint8_t>(result);
	}

	void MOS6502::SEC(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::C, true);
	}

	void MOS6502::SED(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::D, true);
	}

	void MOS6502::SEI(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.setSR(Registers::I, true);
	}

	void MOS6502::SHX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::SHY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::SLO(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::SRE(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::STA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		bus.write(dst, reg.A);
	}

	void MOS6502::STP(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::STX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		bus.write(dst, reg.X);
	}

	void MOS6502::STY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		bus.write(dst, reg.Y);
	}

	void MOS6502::TAS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}

	void MOS6502::TAX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.X = reg.A;
		reg.setSR(reg.X, Registers::Z, Registers::N);
	}

	void MOS6502::TAY(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.Y = reg.A;
		reg.setSR(reg.Y, Registers::Z, Registers::N);
	}

	void MOS6502::TSX(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.X = reg.SP;
		reg.setSR(reg.X, Registers::Z, Registers::N);
	}

	void MOS6502::TXA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A = reg.X;
		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::TXS(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.SP = reg.X;
	}

	void MOS6502::TYA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{
		reg.A = reg.Y;
		reg.setSR(reg.A, Registers::Z, Registers::N);
	}

	void MOS6502::XAA(uint16_t dst, uint16_t value, AddrMode addrMode)
	{}
}
