
#pragma once

#include <iostream>
#include <fstream>

#include "Registers.h"
#include "Bus.inl"
#include "Instruction.h"

namespace _6502
{
	class MOS6502
	{
	public:
		MOS6502();

		void step();

		void load(const char* fileName);
		void load(const char* fileName, uint16_t startAddr);

		Memory mem;
		Registers reg;
		Bus bus;

		void stackPush(uint8_t value);
		void stackPushAddr(uint16_t value);

		uint8_t stackPop();
		uint16_t stackPopAddr();

		void NMI();
		void IRQ();
		void reset();

		static constexpr uint16_t IRQVector = 0xFFFE;
		static constexpr uint16_t RSTVector = 0xFFFC;
		static constexpr uint16_t NMIVector = 0xFFFA;
		static constexpr uint16_t StackBegin = 0x0100;
	private:
		bool differentPages(uint16_t a, uint16_t b) const;
		void addBranchCycles(uint16_t addr);
		void getOPInfo(const Instruction& i, uint16_t& dst, uint16_t& value, bool& bPageCrossed);

		uint8_t _cycles = 0;
		std::vector<Instruction> _instr;

		void _load(const char* fileName);

		void ADC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void AHX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ALR(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ANC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void AND(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ARR(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ASL(uint16_t dst, uint16_t value, AddrMode addrMode);
		void AXS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BCC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BCS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BEQ(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BIT(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BMI(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BNE(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BPL(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BRK(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BVC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void BVS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CLC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CLD(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CLI(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CLV(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CMP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CPX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void CPY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void DCP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void DEC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void DEX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void DEY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void EOR(uint16_t dst, uint16_t value, AddrMode addrMode);
		void INC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void INX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void INY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ISC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void JMP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void JSR(uint16_t dst, uint16_t value, AddrMode addrMode);
		void LAS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void LAX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void LDA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void LDX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void LDY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void LSR(uint16_t dst, uint16_t value, AddrMode addrMode);
		void NOP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ORA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void PHA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void PHP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void PLA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void PLP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void RLA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ROL(uint16_t dst, uint16_t value, AddrMode addrMode);
		void ROR(uint16_t dst, uint16_t value, AddrMode addrMode);
		void RRA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void RTI(uint16_t dst, uint16_t value, AddrMode addrMode);
		void RTS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SAX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SBC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SEC(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SED(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SEI(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SHX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SHY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SLO(uint16_t dst, uint16_t value, AddrMode addrMode);
		void SRE(uint16_t dst, uint16_t value, AddrMode addrMode);
		void STA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void STP(uint16_t dst, uint16_t value, AddrMode addrMode);
		void STX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void STY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TAS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TAX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TAY(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TSX(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TXA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TXS(uint16_t dst, uint16_t value, AddrMode addrMode);
		void TYA(uint16_t dst, uint16_t value, AddrMode addrMode);
		void XAA(uint16_t dst, uint16_t value, AddrMode addrMode);
	};
}

