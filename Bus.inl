
#pragma once

#include <cstdint>

#include "Memory.h"

namespace _6502
{
	class Bus
	{
		Memory* _pMem;

	public:
		inline Bus(Memory* pMem) : _pMem(pMem) {}

		uint16_t addr;
		uint8_t data;

		inline void read()
		{
			data = _pMem->read8(addr);
		}

		inline uint8_t read(uint16_t _addr)
		{
			return (data = _pMem->read8(_addr));
		}

		inline uint16_t readAddr(uint16_t _addr)
		{
			return (addr = ((read(_addr + 1) << 8) | read(_addr)));
		}

		inline void write()
		{
			_pMem->write8(addr, data);
		}

		inline void write(uint8_t value)
		{
			_pMem->write8(addr, (data = value));
		}

		inline void write(uint16_t _addr, uint8_t value)
		{
			_pMem->write8((addr = _addr), (data = value));
		}
	};
}

