#include "Memory.h"

namespace _6502
{
	Memory::Memory(std::size_t nSize)
	{
		_data.resize(nSize);
	}

	uint8_t Memory::read8(uint16_t addr) const
	{
		return _data.at(addr);
	}

	void Memory::write8(uint16_t addr, uint8_t value)
	{
		_data.at(addr) = value;
	}
}
