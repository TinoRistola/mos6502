
#include "Registers.h"

void _6502::Registers::reset()
{
	A = 0;
	X = 0;
	Y = 0;
	SP = 0xFD;
	setSR(0);
}

void _6502::Registers::setSR(uint16_t result, _6502::Registers::Flags flag)
{
	switch (flag)
	{
		case _6502::Registers::C:
			setSR(C, result > 0xFF);
			break;
		case _6502::Registers::Z:
			setSR(Z, result == 0);
			break;
		case _6502::Registers::V:
			setSR(V, (result & 0b01000000) != 0);
			break;
		case _6502::Registers::N:
			setSR(N, (result & 0b10000000) != 0);
			break;
	}
}

void _6502::Registers::setVOnAdd(uint8_t x, uint8_t y, uint8_t result)
{
	setSR(V, !((x ^ y) & 0x80) && ((x ^ result) & 0x80));
}

void _6502::Registers::setVOnSub(uint8_t x, uint8_t y, uint8_t result)
{
	setSR(V, ((x ^ result) & 0x80) && ((x ^ y) & 0x80));
}
