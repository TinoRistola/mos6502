
#pragma once

#include <cstdint>
#include <vector>

namespace _6502
{
	class Memory
	{
	public:
		explicit Memory(std::size_t nSize);

		uint8_t read8(uint16_t addr) const;

		void write8(uint16_t addr, uint8_t value);

		inline uint8_t* get() { return _data.data(); }

	private:
		std::vector<uint8_t> _data;
	};
}

